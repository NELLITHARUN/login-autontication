import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit{

  id: number;
  name: String;
  salary:number;
  address: any;
  Hobbies: any;


  constructor(){
  this.id=101;
  this.name='krishna';
  this.salary=100;

  this.address={streetNo:102,city:'Hyd',state:'Telengana'};
  this.Hobbies=['love','happiness','smile'];

  }
 
 
ngOnInit(){

}

}
