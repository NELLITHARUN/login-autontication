import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ShowingcountriesService} from 'src/app/emp.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{
  emailId:any;
  password:any;
  employees: any;
  
  constructor(private router:Router,private service:ShowingcountriesService) {

    //Dont Delete the Previous Content
  }

  ngOnInit(): void {
  }

  // // buttonSubmit(){
  // //  // one way binding 
  // //   // alert("checking");
  // //   // let str : string = (<HTMLInputElement>document.getElementById("emailid")).value;
  // //   // let str2 : string = (<HTMLInputElement>document.getElementById("password")).value;

  // //   if(this.emailid=="HR" && this.password=="HR"){
  // //     alert("Welcome HR");
  // //   }
  // //   else{
  // //     alert("Invalid credentials");
  // //   }
  // // }

  // login(loginForm: any) {
  //   console.log(loginForm);
  //   if (loginForm.emailId == 'HR' && loginForm.password =='HR') {
  //     alert('Welcome to HR Home Page');
  //     console.log("EmailId : " + this.emailId);
  //     console.log("Password: " + this.password);
  //     this.service.setUserLogin();
  //     this.router.navigate(['products']);

  //   } 
  //   else {
  //     this.employees.forEach((emp:any) => {
  //       if (loginForm.emailId === emp.emailId && loginForm.password === emp.password) {
  //         alert('Welcome to Employee Home Page');    
  //         this.service.setUserLogin();      
  //         this.router.navigate(['products']);
  //       }
  //     });      
  //   }

  // }

  async login(loginForm: any) {
    console.log(loginForm);   

    if (loginForm.emailId === 'HR' && loginForm.password === 'HR') {
      alert('Welcome to HR Home Page');
      this.service.setUserLogin();
         this.router.navigate(['products']);

    } else {

        await this.service.getEmployee(loginForm).then((empData: any) => {
          this.employees = empData;
          console.log(empData);
        });

        if (this.employees != null) {
          this.service.setUserLogin();
          this.router.navigate(['products']);
        } else {
          alert('Invalid Credentials');
          this.router.navigate(['login']);
        }   
    }
  }  
}
