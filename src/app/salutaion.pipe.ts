import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'salutation'
})
export class SalutaionPipe implements PipeTransform {

  salutation:any;
  transform(value: any):any {

if(value=='Male'){
this.salutation='MR';
}
else{
  this.salutation='Miss';
}
    return this.salutation;
  }

}
