import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test2',
  templateUrl: './test2.component.html',
  styleUrls: ['./test2.component.css']
})
export class Test2Component implements OnInit{
  person:any;
  constructor(){
    this.person={
      id:101,
      name:'shank',
      salary:1232.22,
      address:{street:"MG road",city:"Hyderabad",state:"Telangana"},
      hobbies:['cricket','Sleeping','TV','Music']
    }
  }

  ngOnInit() {
  }
  buttonSubmit(){
    console.log("button is clicked")
  }
}