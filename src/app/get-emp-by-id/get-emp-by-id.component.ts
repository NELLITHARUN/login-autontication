import { Component, OnInit } from '@angular/core';
import { ShowingcountriesService} from 'src/app/emp.service'

@Component({
  selector: 'app-get-emp-by-id',
  templateUrl: './get-emp-by-id.component.html',
  styleUrls: ['./get-emp-by-id.component.css']
})

export class GetEmpByIDComponent implements OnInit {
  
  employee: any;
  emp: any;
  constructor(private service:ShowingcountriesService) {
  }
  ngOnInit(){
  }
  
  getEmpById(empByID: any) {
    this.service.getEmployeeById(empByID.empId).subscribe((employee:any)=>{
       this.emp=employee; 
    });
  }
}

