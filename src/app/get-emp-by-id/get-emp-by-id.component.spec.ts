import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetEmpByIDComponent } from './get-emp-by-id.component';

describe('GetEmpByIDComponent', () => {
  let component: GetEmpByIDComponent;
  let fixture: ComponentFixture<GetEmpByIDComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetEmpByIDComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GetEmpByIDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
