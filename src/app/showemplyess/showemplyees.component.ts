import { Component, OnInit } from '@angular/core';
import { ShowingcountriesService } from '../emp.service';

@Component({
  selector: 'app-showemplyees',
  templateUrl: './showemplyees.component.html',
  styleUrls: ['./showemplyees.component.css']
})
export class ShowemplyeesComponent implements OnInit{

employees: any;

constructor
(private service:ShowingcountriesService){
  // this.employees=[
  // {empId:101,empName:"Tharun", salary:5454.54,gender:"male",doj:"11-24-2014",emailid:"tharunnelli@gamil.com",password:"pasword",country:"india"},
  // {empId:102, empName:"Pasha",  salary:5454.54, gender:'Male',   doj:'10-25-2017', emailid:'pasha@gmail.com',  password:'123',country:"india"}, 
  // {empId:103, empName:"Indira", salary:5656.56, gender:'Female', doj:'09-26-2016', emailid:'indira@gmail.com', password:'123',country:"india"}, 
  // {empId:104, empName:"Venkat", salary:6565.65, gender:'Male',   doj:'08-27-2015', emailid:'venkat@gmail.com', password:'123',country:"india"}, 
  // {empId:105, empName:"Gopi",   salary:6767.67, gender:'Male',   doj:'07-28-2014', emailid:'gopi@gmail.com',   password:'123',country:"india"}]
}
ngOnInit(){
  this.service.getAllEmployees().subscribe((empData: any) => {
    this.employees = empData;
    console.log(empData);
  });
}
}
