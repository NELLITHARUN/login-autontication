import { HttpClient } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class ShowingcountriesService {
  userLoginStatus: boolean;


  constructor(private http:HttpClient) { 
    this.userLoginStatus=false;
  }

setUserLogin(){
  this.userLoginStatus=true;
}
getUserLogStatus():boolean{
return this.userLoginStatus;
}
setUserLogout(){
  this.userLoginStatus=false;
}
getAllCountries(){
  return this.http.get('https://restcountries.com/v3.1/all');
}
getAllEmployees(): any {
  return this.http.get('http://localhost:8085/getAllEmployees');
}

getEmployeeById(empId:any): any {
  return this.http.get('http://localhost:8085/getEmployeeById/'+empId);
}

getEmployee(loginForm: any): any {
  return this.http.get('http://localhost:8085/login/' + loginForm.emailId + "/" + loginForm.password).toPromise();
}

}