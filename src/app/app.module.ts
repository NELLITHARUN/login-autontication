import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { Test2Component } from './test2/test2.component';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { ShowemplyeesComponent } from './showemplyess/showemplyees.component';
import { ExpPipe } from './exp.pipe';
import { SalutaionPipe } from './salutaion.pipe';
import { RegisterComponent } from './register/register.component';
import { ProductsComponent } from './products/products.component';
import { HeaderComponent } from './header/header.component';
import { HttpClient,HttpClientModule} from '@angular/common/http';
import { GetallempsComponent } from './getallemps/getallemps.component';
import { RouterModule } from '@angular/router';
import { LogoutComponent } from './logout/logout.component';
import { GetEmpByIDComponent } from './get-emp-by-id/get-emp-by-id.component';


@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    Test2Component,
    LoginComponent,
    ShowemplyeesComponent,
    ExpPipe,
    SalutaionPipe,
    RegisterComponent,
    ProductsComponent,
    HeaderComponent,
    GetallempsComponent,
    LogoutComponent,
    GetEmpByIDComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }