import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ShowemplyeesComponent } from './showemplyess/showemplyees.component';
import { ProductsComponent } from './products/products.component';
import { AuthGuard } from './auth.guard';
import { LogoutComponent } from './logout/logout.component';
import { GetEmpByIDComponent } from './get-emp-by-id/get-emp-by-id.component';

const routes: Routes = [
  {path:"", component:LoginComponent},
  {path:"login", component:LoginComponent},
  {path:"register", component:RegisterComponent},
  {path:"showemployees", canActivate: [AuthGuard],component:ShowemplyeesComponent},
  {path:"products", canActivate: [AuthGuard],component:ProductsComponent},
  {path:"logout",canActivate: [AuthGuard],component:LogoutComponent},
  {path:"getEmpByIDComponent",canActivate: [AuthGuard],component:GetEmpByIDComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 


}
