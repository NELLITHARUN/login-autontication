import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetallempsComponent } from './getallemps.component';

describe('GetallempsComponent', () => {
  let component: GetallempsComponent;
  let fixture: ComponentFixture<GetallempsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetallempsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GetallempsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
